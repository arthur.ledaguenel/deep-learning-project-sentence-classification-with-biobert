"""##Analysis

###On Raw format
"""

import csv
import pandas as pd
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

method_to_analyse = "SplitBalance" #possible values are Raw, oneSentence_random, oneSentence_weighted or SplitBalance

if method_to_analyse=="Raw":

    # From folder ChemProt_

    train_file_path = './ChemProt_Corpus/chemprot_training/chemprot_training_sentences_entities_tagged.tsv'
    test_file_path = './ChemProt_Corpus/chemprot_test_gs/chemprot_test_sentences_entities_tagged_gs.tsv'
    eval_file_path = './ChemProt_Corpus/chemprot_development/chemprot_development_sentences_entities_tagged.tsv'

    train_sentences_tagged = pd.read_csv(train_file_path, sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])
    test_sentences_tagged = pd.read_csv(test_file_path, sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])
    eval_sentences_tagged = pd.read_csv(eval_file_path, sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])

    datasets = [train_sentences_tagged, test_sentences_tagged, eval_sentences_tagged]
    stacked_sentences_tagged = pd.concat(datasets)
    stacked_sentences_tagged.reset_index(inplace=True, drop=True)

    nb_relations_bySentence = stacked_sentences_tagged.groupby(by=['ID_article', 'ID_sentence'])["label"].sum()
    plt.xlabel("Nombre de relations")
    plt.ylabel("Nombres de phrases")
    plt.hist(nb_relations_bySentence.values, bins=80, log=True, range=(0,40), color="b")

    nb_versions_bySentence = stacked_sentences_tagged.groupby(by=['ID_article', 'ID_sentence'])["label"].count()
    plt.xlabel("Nombre de combinaisons")
    plt.ylabel("Nombres de phrases")
    plt.hist(nb_versions_bySentence.values, bins=200, log=True, range=(0,500), color="b")

    print("Proportion globale de label 1 : ", stacked_sentences_tagged["label"].mean()*100, "%")
    print("Proportion de classe 1 dans le train set : ", train_sentences_tagged["label"].mean()*100, "%")
    print("Proportion de classe 1 dans le test set : ", test_sentences_tagged["label"].mean()*100, "%")
    print("Proportion de classe 1 dans le validation set : ", eval_sentences_tagged["label"].mean()*100, "%")

elif method_to_analyse=="oneSentence_random":
    """### One sentence random sampling format"""

    # From folder ChemProt_

    train_file_path = './ChemProt_Glue/oneSentence_random/train.tsv'
    test_file_path = './ChemProt_Glue/oneSentence_random/test.tsv'
    eval_file_path = './ChemProt_Glue/oneSentence_random/val.tsv'

    train_sentences_tagged = pd.read_csv(train_file_path, sep='\t', header=None, names=['sentence', 'label'])
    test_sentences_tagged = pd.read_csv(test_file_path, sep='\t', header=None, names=['sentence', 'label'])
    eval_sentences_tagged = pd.read_csv(eval_file_path, sep='\t', header=None, names=['sentence', 'label'])

    datasets = [train_sentences_tagged, test_sentences_tagged, eval_sentences_tagged]
    stacked_sentences_tagged = pd.concat(datasets)
    stacked_sentences_tagged.reset_index(inplace=True, drop=True)

    print("Proportion globale de label 1 : ", stacked_sentences_tagged["label"].mean()*100, "%")
    print("Proportion de classe 1 dans le train set : ", train_sentences_tagged["label"].mean()*100, "%")
    print("Proportion de classe 1 dans le test set : ", test_sentences_tagged["label"].mean()*100, "%")
    print("Proportion de classe 1 dans le validation set : ", eval_sentences_tagged["label"].mean()*100, "%")

elif method_to_analyse=="oneSentence_weighted":

    """###One sentence weighted sampling format"""

    # From folder ChemProt_

    train_file_path = './ChemProt_Glue/oneSentence_weighted/train.tsv'
    test_file_path = './ChemProt_Glue/oneSentence_weighted/test.tsv'
    eval_file_path = './ChemProt_Glue/oneSentence_weighted/val.tsv'

    train_sentences_tagged = pd.read_csv(train_file_path, sep='\t', header=None, names=['sentence', 'label'])
    test_sentences_tagged = pd.read_csv(test_file_path, sep='\t', header=None, names=['sentence', 'label'])
    eval_sentences_tagged = pd.read_csv(eval_file_path, sep='\t', header=None, names=['sentence', 'label'])

    datasets = [train_sentences_tagged, test_sentences_tagged, eval_sentences_tagged]
    stacked_sentences_tagged = pd.concat(datasets)
    stacked_sentences_tagged.reset_index(inplace=True, drop=True)

    print("Proportion globale de label 1 : ", stacked_sentences_tagged["label"].mean()*100, "%")
    print("Proportion de classe 1 dans le train set : ", train_sentences_tagged["label"].mean()*100, "%")
    print("Proportion de classe 1 dans le test set : ", test_sentences_tagged["label"].mean()*100, "%")
    print("Proportion de classe 1 dans le validation set : ", eval_sentences_tagged["label"].mean()*100, "%")

elif method_to_analyse=="SplitBalance":

    """###Split balanced format"""

    # From folder ChemProt_

    train_file_path = './ChemProt_Glue/SplitBalanced/train.tsv'
    test_file_path = './ChemProt_Glue/SplitBalanced/test.tsv'
    eval_file_path = './ChemProt_Glue/SplitBalanced/val.tsv'

    train_sentences_tagged = pd.read_csv(train_file_path, sep='\t', header=None, names=['sentence', 'label'])
    test_sentences_tagged = pd.read_csv(test_file_path, sep='\t', header=None, names=['sentence', 'label'])
    eval_sentences_tagged = pd.read_csv(eval_file_path, sep='\t', header=None, names=['sentence', 'label'])

    datasets = [train_sentences_tagged, test_sentences_tagged, eval_sentences_tagged]
    stacked_sentences_tagged = pd.concat(datasets)
    stacked_sentences_tagged.reset_index(inplace=True, drop=True)

    print("Proportion globale de label 1 : ", stacked_sentences_tagged["label"].mean()*100, "%")
    print("Proportion de classe 1 dans le train set : ", train_sentences_tagged["label"].mean()*100, "%")
    print("Proportion de classe 1 dans le test set : ", test_sentences_tagged["label"].mean()*100, "%")
    print("Proportion de classe 1 dans le validation set : ", eval_sentences_tagged["label"].mean()*100, "%")