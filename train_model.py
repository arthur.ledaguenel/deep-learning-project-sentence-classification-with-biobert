import numpy as np
from torch import nn
import os
from sklearn.metrics import accuracy_score, f1_score, recall_score, precision_score
import pandas as pd

from datasets import load_dataset, ClassLabel, Value

from transformers import (
    AutoConfig,
    AutoModelForSequenceClassification,
    AutoTokenizer,
    EvalPrediction,
    Trainer,
    TrainingArguments,
    set_seed,
)

def metrics(preds, labels):
  acc = accuracy_score(y_true=labels, y_pred=preds)
  f1 = f1_score(y_true=labels, y_pred=preds)
  recall = recall_score(y_true=labels, y_pred=preds)
  precision = precision_score(y_true=labels, y_pred=preds)
  return {
      "f1": f1,
      "acc": acc,
      "precision": precision,
      "recall": recall
      }


def glue_compute_metrics(preds, labels):
  assert len(preds) == len(labels)
  return metrics(preds, labels)

def compute_metrics_fn(p: EvalPrediction):
  preds = np.argmax(p.predictions, axis=1)
  return glue_compute_metrics(preds, p.label_ids)

"""Define hyperparameters"""

params = {"preprocessing_method":"SplitBalanced", #possible values are : raw, oneSentence_random, oneSentence_weighted or SplitBalanced
          "validation_sampled":False,
          "num_epochs":5,
          "keep_best_model":True,
          "weight_decay":0,
          "batch_size":32,
          "custom_loss":True,
          "weighted_loss":True,
          "weight_param":-0.15,
          "raw_test":False,
          "additional_layer":False}

"""Load dataset ChemProt"""

def load_data(params):
  preprocessing_method = params["preprocessing_method"]

  data_files_train = ['./ChemProt_Glue/{}/train.tsv'.format(preprocessing_method)]
  data_files_val = ['./ChemProt_Glue/{}/val.tsv'.format(preprocessing_method)]

  train_data = load_dataset('csv', data_files=data_files_train, delimiter='\t', column_names=["sentence", "labels"])
  val_data = load_dataset('csv', data_files=data_files_val, delimiter='\t', column_names=["sentence", "labels"])

  if params["raw_test"]:
    test_data = load_dataset('csv', data_files='./ChemProt_Glue/Raw/test.tsv', delimiter='\t', column_names=["sentence", "labels"])
  else:
    data_files_test = ['./ChemProt_Glue/{}/test.tsv'.format(preprocessing_method)]
    test_data = load_dataset('csv', data_files=data_files_test, delimiter='\t', column_names=["sentence", "labels"])

    return train_data, val_data, test_data

"""Preprocess data"""

def preprocess_data(train_data, val_data, test_data):

  tokenizer = AutoTokenizer.from_pretrained("dmis-lab/biobert-base-cased-v1.1")

  padding = "max_length"
  max_seq_length = 128
  max_seq_length = min(max_seq_length, tokenizer.model_max_length)

  def preprocess_function(examples):
    result = tokenizer(examples["sentence"], padding=padding, max_length=max_seq_length, truncation=True)
    return result

  # Tokenize the texts

  train_data = train_data.map(preprocess_function, batched=True, load_from_cache_file=False)
  test_data = test_data.map(preprocess_function, batched=True, load_from_cache_file=False)
  val_data = val_data.map(preprocess_function, batched=True, load_from_cache_file=False)

  train_dataset = train_data["train"]
  test_dataset = test_data["train"]
  val_dataset = val_data["train"]

  # Setting label columns to ClassLabel type

  train_features = train_dataset.features.copy()
  train_features["labels"] = ClassLabel(num_classes = 2)
  train_dataset = train_dataset.cast(train_features)

  test_features = test_dataset.features.copy()
  test_features["labels"] = ClassLabel(num_classes = 2)
  test_dataset = test_dataset.cast(test_features)

  val_features = val_dataset.features.copy()
  val_features["labels"] = ClassLabel(num_classes = 2)
  val_dataset = val_dataset.cast(val_features)

  return train_dataset, val_dataset, test_dataset, tokenizer


"""Load model & train"""

def load_model(params):

  set_seed(42)

  config = AutoConfig.from_pretrained("bert-base-cased", num_labels=2, finetuning_task="SST-2")

  model = AutoModelForSequenceClassification.from_pretrained(
          "dmis-lab/biobert-base-cased-v1.1",
          from_tf=False,
          config=config)

# OPTIONAL : execute to add more hidden layers to the end classifier

  if params["additional_layer"]:
    from torch import nn
    from collections import OrderedDict

    n_inputs = model.classifier.in_features
    n_outputs = model.classifier.out_features
    hidden_layer_dim = 512
    dropout = 0.1 

    # add more layers as required
    new_classifier = nn.Sequential(OrderedDict([
        ('classification_layer_1', nn.Linear(n_inputs, hidden_layer_dim)),
        ('batch_normalization', nn.BatchNorm1d(hidden_layer_dim)),
        ('activation_layer', nn.ReLU()),
        ('dropout', nn.Dropout(p = dropout, inplace=False)),
        ('classification_layer_2', nn.Linear(hidden_layer_dim, n_outputs))
    ]))

    model.classifier = new_classifier

  return model

# Training args for ChemProt

def train_model(params):

  train_data, val_data, test_data = load_data(params)
  train_dataset, val_dataset, test_dataset, tokenizer = preprocess_data(train_data, val_data, test_data)
  model = load_model(params)

  output_path = "./ChemProt_outputs/{}/base".format(params["preprocessing_method"])

  training_args = TrainingArguments(output_dir=output_path,
                                    num_train_epochs = params["num_epochs"],
                                    per_device_train_batch_size = params["batch_size"],
                                    weight_decay=params["weight_decay"],
                                    #overwrite_output_dir=True,
                                    do_train = True,
                                    evaluation_strategy  = "steps",
                                    eval_steps = 5000//params["batch_size"],
                                    load_best_model_at_end=params["keep_best_model"],
                                    metric_for_best_model="f1",
                                    )

  # Test with modified trainer for weighted CrossEntropyLoss

  if params["custom_loss"]:

    from torch import FloatTensor

    weights = [0.5+params["weight_param"], 0.5-params["weight_param"]]
    weights_tensor = FloatTensor(weights).cuda()

    from torch.nn import CrossEntropyLoss

    class MyTrainer(Trainer):
        def compute_loss(self, model, inputs, return_outputs=False):
            
            if "labels" in inputs:
              labels = inputs.pop("labels")
            
            outputs = model(**inputs)
            logits = outputs.logits

            if params["weighted_loss"]:
              loss_function = CrossEntropyLoss(weight = weights_tensor)
            else:
              loss_function = CrossEntropyLoss()

            if self.args.past_index >= 0:
                self._past = outputs[self.args.past_index]

            if labels is not None:
                loss = loss_function(logits, labels)
            else:
                # We don't use .loss here since the model may return tuples instead of ModelOutput.
                loss = outputs["loss"] if isinstance(outputs, dict) else outputs[0]

            return (loss, outputs) if return_outputs else loss

    trainer = MyTrainer(
            model=model,
            args=training_args,
            train_dataset=train_dataset,
            eval_dataset=val_dataset,
            compute_metrics=compute_metrics_fn,
            tokenizer=tokenizer,
        )

  else:
    trainer = Trainer(
          model=model,
          args=training_args,
          train_dataset=train_dataset,
          eval_dataset=val_dataset,
          compute_metrics=compute_metrics_fn,
          tokenizer=tokenizer,
      )

  trainer.train()

  return trainer, model, test_dataset

trainer, model, test_dataset = train_model(params)

"""###Evaluate and actualize results file"""

results = trainer.evaluate(test_dataset)
results["eval_f1"]

new_results = {}

for param, value in params.items():
  new_results[param] = [value]

new_results["F1_score"] = [results["eval_f1"]]

new_results_df = pd.DataFrame.from_dict(new_results)
past_results_df = pd.read_csv("./ChemProt_outputs/results.tsv", sep='\t', header=0)
total_results_df = pd.concat([past_results_df, new_results_df]).reset_index(drop=True)
total_results_df

total_results_df.to_csv(path_or_buf="./ChemProt_outputs/results.tsv", sep='\t', columns=['preprocessing_method', 'validation_sampled', 'num_epochs', 'keep_best_model', 'weight_decay', 'additional_layer', 'batch_size', 'custom_loss', 'weighted_loss', 'weight_param', 'raw_test', 'F1_score'], header=True, index=False)