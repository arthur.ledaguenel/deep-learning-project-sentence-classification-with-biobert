import csv
import pandas as pd
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

split = 'training'
#split = 'test'
#split = 'development'

def preprocess_sentences(split):

  if split=='test':

    path = './ChemProt_Corpus/chemprot_test_gs/'

    abstracts_file = path + "chemprot_test_abstracts_gs.tsv"
    raw_sentences_file = path + "chemprot_test_sentences_raw_gs.tsv"
    entities_file = path + "chemprot_test_entities_gs.tsv"
    relations_file = path + "chemprot_test_gold_standard.tsv"
    tagged_entities_file = path + "chemprot_test_sentences_entities_tagged_gs.tsv"

  else:

    path = './ChemProt_Corpus/chemprot_{}/'.format(split)

    abstracts_file = path + "chemprot_{}_abstracts.tsv".format(split)
    raw_sentences_file = path + "chemprot_{}_sentences_raw.tsv".format(split)
    entities_file = path + "chemprot_{}_entities.tsv".format(split)
    relations_file = path + "chemprot_{}_gold_standard.tsv".format(split)
    tagged_entities_file = path + "chemprot_{}_sentences_entities_tagged.tsv".format(split)

  """## Split text into sentences"""

  sentences = {}

  with open(abstracts_file, newline='') as tsvfile:
    lines = csv.reader(tsvfile, delimiter='\t')
    for row in lines:
      text = row[1] + " " + row[2]
      sentences[row[0]] = text.split(sep=". ")

  with open(raw_sentences_file, 'wt') as tsvfile:
    writer = csv.writer(tsvfile, delimiter='\t')
    writer.writerow(['ID_article', 'ID_sentence', 'start_char', 'sentence'])
    for id, text in sentences.items():
      start_char=0
      nb_sentences = len(text)
      for i in range(nb_sentences):
        writer.writerow([id, i, start_char, text[i]])
        start_char = start_char + len(text[i])+2

  """## Identify entities in sentences

  ### Create a list of starting characters for all sentences in an article
  """

  sentences = pd.read_csv(raw_sentences_file, sep='\t', header=0)

  start_char_list = sentences.groupby(by="ID_article")['start_char'].apply(list).reset_index(name='start_char_list')

  """### Identify to which sentence each entity belongs
  Plus two features giving the starting an ending characters of each entity inside the sentence
  """

  entities = pd.read_csv(entities_file, sep='\t', header=None, names=['ID_article', 'ID_entity', 'Category', 'start_char', 'end_char', 'entity'])

  entities_extended = entities.set_index('ID_article').join(start_char_list.set_index('ID_article')).reset_index()

  def fetch_ID_sentence(cell):
    start_char = cell['start_char']
    start_char_list = cell['start_char_list']

    l = len(start_char_list)
    ind_m = 0
    ind_M = l-1
    p=l//2

    while ind_M-ind_m > 1:
      if start_char_list[p] <= start_char:
        ind_m = p
        p = (ind_M+ind_m)//2

      else:
        ind_M = p
        p = (ind_M+ind_m)//2


    return ind_m

  def fetch_start_char_in_sentence(cell):
    ID_sentence = cell['ID_sentence']
    start_char_list = cell['start_char_list']
    start_char = cell['start_char']
    return start_char - start_char_list[ID_sentence]

  def fetch_end_char_in_sentence(cell):
    ID_sentence = cell['ID_sentence']
    start_char_list = cell['start_char_list']
    end_char = cell['end_char']
    return end_char - start_char_list[ID_sentence]

  entities_extended["ID_sentence"] = entities_extended.apply(fetch_ID_sentence, axis=1)
  entities_extended["start_char_in_sentence"] = entities_extended.apply(fetch_start_char_in_sentence, axis=1)
  entities_extended["end_char_in_sentence"] = entities_extended.apply(fetch_end_char_in_sentence, axis=1)


  """## Identify relations in sentences"""

  relations = pd.read_csv(relations_file, sep='\t', header=None, names=["ID_article", "cat_relation", "Arg1", "Arg2"])

  def clear_arg1(cell):
    return cell["Arg1"][5:]

  def clear_arg2(cell):
    return cell["Arg2"][5:]

  relations["Arg1"] = relations.apply(clear_arg1, axis=1)
  relations["Arg2"] = relations.apply(clear_arg2, axis=1)

  # Join information coming from both entities

  relations_extended = relations.join(entities_extended[["ID_article", "ID_entity", "ID_sentence", "Category"]].set_index(["ID_article", "ID_entity"]), on = ["ID_article", "Arg1"])
  relations_extended = relations_extended.join(entities_extended[["ID_article", "ID_entity", "ID_sentence", "Category"]].set_index(["ID_article", "ID_entity"]), on = ["ID_article", "Arg2"], lsuffix='_Arg1', rsuffix='_Arg2')

  relations_extended["couple_entities"] = '[' + relations_extended["Arg1"] + "," +relations_extended["Arg2"] + ']'

  # Check percentage of relations that take place in one sentence

  (relations_extended["ID_sentence_Arg1"]==relations_extended["ID_sentence_Arg2"]).mean()

  """## Group entities and relations infos

  ###Group entities infos by sentence
  Plot #entities/article (only for sentences containing entities)
  """

  entities_bySentence = entities_extended.groupby(by=["ID_article", "ID_sentence"])["ID_entity"].apply(list).reset_index(name='entities')
  entities_bySentence["list_start_char_in_sentence"] = entities_extended.groupby(by=["ID_article", "ID_sentence"])["start_char_in_sentence"].apply(list).reset_index(name='start_char_in_sentence')["start_char_in_sentence"]
  entities_bySentence["list_end_char_in_sentence"] = entities_extended.groupby(by=["ID_article", "ID_sentence"])["end_char_in_sentence"].apply(list).reset_index(name='end_char_in_sentence')["end_char_in_sentence"]
  entities_bySentence["list_categories"] = entities_extended.groupby(by=["ID_article", "ID_sentence"])["Category"].apply(list).reset_index(name='Category')["Category"]

  def nb_entities(cell):
    return len(cell['entities'])

  entities_bySentence['nb_entities'] = entities_bySentence.apply(nb_entities, axis=1)

  # We check that all sentences have the same number of entities than starting characters

  def nb_start_char(cell):
    return len(cell['list_start_char_in_sentence'])

  ((entities_bySentence['nb_entities'] == entities_bySentence.apply(nb_start_char, axis=1))==False).sum()

  """### Create a dataframe with the relations' entity couples for each sentence in an article"""

  ratio = relations_extended[relations_extended["ID_sentence_Arg1"]!=relations_extended["ID_sentence_Arg2"]].shape[0]/relations_extended.shape[0]
  print("Pourcentage de relations cross-sentences : ", ratio)

  relations_filtred = relations_extended[relations_extended["ID_sentence_Arg1"]==relations_extended["ID_sentence_Arg2"]]
  couple_entities_bySentence = relations_filtred.groupby(by=["ID_article", "ID_sentence_Arg1"])["couple_entities"].apply(list).reset_index(name='couple_entities')


  """## Tagged sentences generation

  ###Enrich sentences dataframe with lists of entities, start and end char, categories, etc.
  """

  sentences_extended = sentences.join(other=entities_bySentence.set_index(["ID_article", "ID_sentence"]), on = ["ID_article", "ID_sentence"])
  sentences_extended = sentences_extended.join(other = couple_entities_bySentence.set_index(["ID_article", "ID_sentence_Arg1"]), on = ["ID_article", "ID_sentence"])


  sentences_extended["nb_entities"].fillna(0, inplace = True)
  sentences_extended["entities"].fillna('NoEntities', inplace = True)
  sentences_extended["list_start_char_in_sentence"].fillna('NoEntities', inplace = True)
  sentences_extended["list_end_char_in_sentence"].fillna('NoEntities', inplace = True)
  sentences_extended["list_categories"].fillna('NoEntities', inplace = True)
  sentences_extended["couple_entities"].fillna('NoRelations', inplace = True)
  sentences_extended["nb_entities"] = sentences_extended["nb_entities"].astype('int')


  """###Generate dataset with tagged entities in sentences, and relations identified"""

  def test_replacement_meta(writer):  
    def test_replacement(cell):
      
      entities = cell["entities"]
      
      if entities != "NoEntities":

        ID_article = cell["ID_article"]
        ID_sentence = cell["ID_sentence"]
        text = cell["sentence"]
        list_start_char_in_sentence = cell["list_start_char_in_sentence"]
        list_end_char_in_sentence = cell["list_end_char_in_sentence"]
        list_categories = cell["list_categories"]
        nb_entities = cell["nb_entities"]
        couple_entities = cell["couple_entities"]
        relation = 0
        

        for i in range(nb_entities-1):
          start_char_1 = list_start_char_in_sentence[i]
          end_char_1 = list_end_char_in_sentence[i]
          cat_1 = list_categories[i]

          text_1 = text[:start_char_1] + "@" + cat_1 + text[end_char_1:]

          compensation = len(cat_1) + 1 - (end_char_1 - start_char_1)
          
          for j in range(i+1,nb_entities):
            start_char_2 = list_start_char_in_sentence[j]
            end_char_2 = list_end_char_in_sentence[j]
            
            if start_char_2>start_char_1:
              start_char_2 = start_char_2 + compensation
              end_char_2 = end_char_2 + compensation
            
            cat_2 = list_categories[j]

            text_2 = text_1[:start_char_2] + "@" + cat_2 + text_1[end_char_2:]

            if couple_entities != "NoRelations":
              couple = "[" + entities[i] + "," + entities[j] + "]"
              if couple in couple_entities:
                relation = 1
              else:
                relation = 0

            writer.writerow([ID_article, ID_sentence, entities[i], entities[j], text_2, relation])
          
    return test_replacement

  with open(tagged_entities_file, 'wt') as out_tsvfile:
    tsv_writer = csv.writer(out_tsvfile, delimiter='\t')
    sentences_extended.apply(test_replacement_meta(tsv_writer), axis=1)



"""##Analysis

###On Raw format
"""

# From folder ChemProt_

train_file_path = './ChemProt_Corpus/chemprot_training/chemprot_training_sentences_entities_tagged.tsv'
test_file_path = './ChemProt_Corpus/chemprot_test_gs/chemprot_test_sentences_entities_tagged_gs.tsv'
eval_file_path = './ChemProt_Corpus/chemprot_development/chemprot_development_sentences_entities_tagged.tsv'

train_sentences_tagged = pd.read_csv(train_file_path, sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])
test_sentences_tagged = pd.read_csv(test_file_path, sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])
eval_sentences_tagged = pd.read_csv(eval_file_path, sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])

datasets = [train_sentences_tagged, test_sentences_tagged, eval_sentences_tagged]
stacked_sentences_tagged = pd.concat(datasets)
stacked_sentences_tagged.reset_index(inplace=True, drop=True)

nb_relations_bySentence = stacked_sentences_tagged.groupby(by=['ID_article', 'ID_sentence'])["label"].sum()
plt.xlabel("Nombre de relations")
plt.ylabel("Nombres de phrases")
plt.hist(nb_relations_bySentence.values, bins=80, log=True, range=(0,40), color="b")

nb_versions_bySentence = stacked_sentences_tagged.groupby(by=['ID_article', 'ID_sentence'])["label"].count()
plt.xlabel("Nombre de combinaisons")
plt.ylabel("Nombres de phrases")
plt.hist(nb_versions_bySentence.values, bins=200, log=True, range=(0,500), color="b")

print("Proportion globale de label 1 : ", stacked_sentences_tagged["label"].mean()*100, "%")
print("Proportion de classe 1 dans le train set : ", train_sentences_tagged["label"].mean()*100, "%")
print("Proportion de classe 1 dans le test set : ", test_sentences_tagged["label"].mean()*100, "%")
print("Proportion de classe 1 dans le validation set : ", eval_sentences_tagged["label"].mean()*100, "%")

"""### One sentence random sampling format"""

# From folder ChemProt_

train_file_path = './ChemProt_Glue/oneSentence_random/train.tsv'
test_file_path = './ChemProt_Glue/oneSentence_random/test.tsv'
eval_file_path = './ChemProt_Glue/oneSentence_random/val.tsv'

train_sentences_tagged = pd.read_csv(train_file_path, sep='\t', header=None, names=['sentence', 'label'])
test_sentences_tagged = pd.read_csv(test_file_path, sep='\t', header=None, names=['sentence', 'label'])
eval_sentences_tagged = pd.read_csv(eval_file_path, sep='\t', header=None, names=['sentence', 'label'])

datasets = [train_sentences_tagged, test_sentences_tagged, eval_sentences_tagged]
stacked_sentences_tagged = pd.concat(datasets)
stacked_sentences_tagged.reset_index(inplace=True, drop=True)

print("Proportion globale de label 1 : ", stacked_sentences_tagged["label"].mean()*100, "%")
print("Proportion de classe 1 dans le train set : ", train_sentences_tagged["label"].mean()*100, "%")
print("Proportion de classe 1 dans le test set : ", test_sentences_tagged["label"].mean()*100, "%")
print("Proportion de classe 1 dans le validation set : ", eval_sentences_tagged["label"].mean()*100, "%")

"""###One sentence weighted sampling format"""

# From folder ChemProt_

train_file_path = './ChemProt_Glue/oneSentence_weighted/train.tsv'
test_file_path = './ChemProt_Glue/oneSentence_weighted/test.tsv'
eval_file_path = './ChemProt_Glue/oneSentence_weighted/val.tsv'

train_sentences_tagged = pd.read_csv(train_file_path, sep='\t', header=None, names=['sentence', 'label'])
test_sentences_tagged = pd.read_csv(test_file_path, sep='\t', header=None, names=['sentence', 'label'])
eval_sentences_tagged = pd.read_csv(eval_file_path, sep='\t', header=None, names=['sentence', 'label'])

datasets = [train_sentences_tagged, test_sentences_tagged, eval_sentences_tagged]
stacked_sentences_tagged = pd.concat(datasets)
stacked_sentences_tagged.reset_index(inplace=True, drop=True)

print("Proportion globale de label 1 : ", stacked_sentences_tagged["label"].mean()*100, "%")
print("Proportion de classe 1 dans le train set : ", train_sentences_tagged["label"].mean()*100, "%")
print("Proportion de classe 1 dans le test set : ", test_sentences_tagged["label"].mean()*100, "%")
print("Proportion de classe 1 dans le validation set : ", eval_sentences_tagged["label"].mean()*100, "%")

"""###Split balanced format"""

# From folder ChemProt_

train_file_path = './ChemProt_Glue/SplitBalanced/train.tsv'
test_file_path = './ChemProt_Glue/SplitBalanced/test.tsv'
eval_file_path = './ChemProt_Glue/SplitBalanced/val.tsv'

train_sentences_tagged = pd.read_csv(train_file_path, sep='\t', header=None, names=['sentence', 'label'])
test_sentences_tagged = pd.read_csv(test_file_path, sep='\t', header=None, names=['sentence', 'label'])
eval_sentences_tagged = pd.read_csv(eval_file_path, sep='\t', header=None, names=['sentence', 'label'])

datasets = [train_sentences_tagged, test_sentences_tagged, eval_sentences_tagged]
stacked_sentences_tagged = pd.concat(datasets)
stacked_sentences_tagged.reset_index(inplace=True, drop=True)

print("Proportion globale de label 1 : ", stacked_sentences_tagged["label"].mean()*100, "%")
print("Proportion de classe 1 dans le train set : ", train_sentences_tagged["label"].mean()*100, "%")
print("Proportion de classe 1 dans le test set : ", test_sentences_tagged["label"].mean()*100, "%")
print("Proportion de classe 1 dans le validation set : ", eval_sentences_tagged["label"].mean()*100, "%")