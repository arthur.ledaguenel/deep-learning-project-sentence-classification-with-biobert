import pandas as pd

import numpy as np
import matplotlib.pyplot as plt

results = pd.read_csv("./ChemProt_outputs/results.tsv", sep='\t', header=0)

analysis_to_run = "weighted_loss" #possible values are baseline, batch_size, weight_decay, additional_layer or weighted_loss 
"""Comapre baseline vs. keep best model"""

if analysis_to_run=="baseline":

    baseline = (results["validation_sampled"]==False)&(results["num_epochs"]==3)&(results["keep_best_model"]==False)&(results["weight_decay"]==0)&(results["additional_layer"]==False)&(results["batch_size"]==8)&(results["custom_loss"]==False)&(results["raw_test"]==False)
    best_model_base = (results["validation_sampled"]==False)&(results["num_epochs"]==5)&(results["keep_best_model"]==True)&(results["additional_layer"]==False)&(results["batch_size"]==8)&(results["weight_decay"]==0)&(results["custom_loss"]==False)&(results["raw_test"]==False)
    baseline_results = results[baseline].sort_values(by="F1_score")
    best_model_results = results[best_model_base].sort_values(by="F1_score")

    labels = baseline_results.preprocessing_method.unique()
    baseline_values = baseline_results.F1_score.values.round(decimals=3)
    best_model_values = best_model_results.F1_score.values.round(decimals=3)

    x = np.arange(len(labels))  # the label locations
    width = 0.15  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width, baseline_values, width, label='Baseline')
    rects2 = ax.bar(x + width, best_model_values, width, label='Best model')

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('F1 scores')
    ax.set_title('Comapre baseline vs. best model')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()

    plt.ylim([0, 1])

    def autolabel(rects):
        """Attach a text label above each bar in *rects*, displaying its height."""
        for rect in rects:
            height = rect.get_height()
            ax.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center', va='bottom')


    autolabel(rects1)
    autolabel(rects2)

    fig.tight_layout()

    plt.show()


elif analysis_to_run=="batch_size":

    """## Analyse du batch_size"""

    batch_size_analysis_oneSentence_weighted = (results["preprocessing_method"]=="oneSentence_weighted")&(results["validation_sampled"]==False)&(results["num_epochs"]==5)&(results["keep_best_model"]==True)&(results["additional_layer"]==False)&(results["weight_decay"]==0)&(results["custom_loss"]==False)&(results["raw_test"]==False)
    batch_size_analysis_SplitBalanced = (results["preprocessing_method"]=="SplitBalanced")&(results["validation_sampled"]==False)&(results["num_epochs"]==5)&(results["keep_best_model"]==True)&(results["additional_layer"]==False)&(results["weight_decay"]==0)&(results["custom_loss"]==False)&(results["raw_test"]==False)


    fig= plt.figure(figsize=(5,10))
    ax = fig.add_subplot(3, 1, (1, 2))
    ax.set_xscale('log', basex=2)
    plt.plot('batch_size', 'F1_score', data=results[batch_size_analysis_oneSentence_weighted].sort_values(by="batch_size"), label="oneSentence_weighted")
    plt.plot('batch_size', 'F1_score', data=results[batch_size_analysis_SplitBalanced].sort_values(by="batch_size"), label="SplitBalanced")

    ax.set_ylabel('F1 scores')
    ax.set_xlabel('Batch size')

    plt.legend(loc="center")
    plt.show()

elif analysis_to_run=="weight_decay":

    """## Analyse du weight decay"""

    weight_decay_analysis_oneSentence_random = (results["preprocessing_method"]=="oneSentence_random")&(results["validation_sampled"]==False)&(results["num_epochs"]==5)&(results["keep_best_model"]==True)&(results["additional_layer"]==False)&(results["custom_loss"]==False)&(results["raw_test"]==False)
    weight_decay_analysis_oneSentence_weighted = (results["preprocessing_method"]=="oneSentence_weighted")&(results["validation_sampled"]==False)&(results["num_epochs"]==5)&(results["keep_best_model"]==True)&(results["additional_layer"]==False)&(results["batch_size"]==64)&(results["custom_loss"]==False)&(results["raw_test"]==False)
    weight_decay_analysis_SplitBalanced = (results["preprocessing_method"]=="SplitBalanced")&(results["validation_sampled"]==False)&(results["num_epochs"]==5)&(results["keep_best_model"]==True)&(results["additional_layer"]==False)&(results["batch_size"]==32)&(results["custom_loss"]==False)&(results["raw_test"]==False)


    fig= plt.figure(figsize=(5,10))
    ax = fig.add_subplot(3, 1, (1, 2))
    ax.set_xscale('log')
    plt.plot('weight_decay', 'F1_score', data=results[weight_decay_analysis_SplitBalanced].sort_values(by="weight_decay"), label="SplitBalanced")
    plt.plot('weight_decay', 'F1_score', data=results[weight_decay_analysis_oneSentence_weighted].sort_values(by="weight_decay"), label="oneSentence_weighted")
    plt.plot('weight_decay', 'F1_score', data=results[weight_decay_analysis_oneSentence_random].sort_values(by="weight_decay"), label="oneSentence_random")

    ax.set_ylabel('F1 scores')
    ax.set_xlabel('Weight decay')

    plt.legend(loc="center right")
    plt.show()

elif analysis_to_run=="additional_layer":

    """## Addition of hidden layer"""

    compare_addition_layer = (results["preprocessing_method"]=="SplitBalanced")&(results["validation_sampled"]==False)&(results["weight_decay"]==0)&(results["batch_size"]==32)

    comparison = results[compare_addition_layer]

    labels = ['SplitBalanced']
    AdditionalLayer = results.iloc[12].F1_score.round(decimals=4)
    noAdditionalLayer = results.iloc[25].F1_score.round(decimals=4)

    x = np.arange(len(labels))  # the label locations
    width = 0.1  # the width of the bars

    fig= plt.figure(figsize=(4,6))
    ax = fig.subplots()
    rects1 = ax.bar(x - width, AdditionalLayer, width, label='Additional Layer')
    rects2 = ax.bar(x + width, noAdditionalLayer, width, label='No Additional Layer')

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('F1 scores')
    ax.set_title('Comapre baseline vs. best model')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()

    plt.ylim([0, 1])

    ax.set_title('Additional layer impact')
    ax.set_ylabel('F1 scores')

    def autolabel(rects):
            """Attach a text label above each bar in *rects*, displaying its height."""
            for rect in rects:
                height = rect.get_height()
                ax.annotate('{}'.format(height),
                            xy=(rect.get_x() + rect.get_width() / 2, height),
                            xytext=(0, 3),  # 3 points vertical offset
                            textcoords="offset points",
                            ha='center', va='bottom')

    autolabel(rects1)
    autolabel(rects2)

    fig.tight_layout()



    plt.show()

elif analysis_to_run=="weighted_loss":

    """## Weighted CrossEntropy"""

    results_loss_weighted = results[results["weighted_loss"]==True]

    fig= plt.figure(figsize=(5,10))
    ax = fig.add_subplot(3, 1, (1, 2))
    plt.plot('weight_param', 'F1_score', data=results_loss_weighted.sort_values(by="weight_param"), label="SplitBalanced")


    ax.set_ylabel('F1 scores')
    ax.set_xlabel('Weight param')

    plt.title("Testing weighted loss with different weights")

    plt.legend(loc="upper left")
    plt.show()