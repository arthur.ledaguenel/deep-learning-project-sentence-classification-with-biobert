import csv
import pandas as pd
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt


"""## GLUE format

###Raw format
No changes, just deleting useless columns for learning and shuffling the dataset
"""

raw_format_path_dict = {}
raw_format_path_dict["train"] = {"input" : "./ChemProt_Corpus/chemprot_training/chemprot_training_sentences_entities_tagged.tsv",
                      "output" : "./ChemProt_Glue/Raw/train.tsv"}

raw_format_path_dict["test"] = {"input" : "./ChemProt_Corpus/chemprot_test_gs/chemprot_test_sentences_entities_tagged_gs.tsv",
                      "output" : "./ChemProt_Glue/Raw/test.tsv"}

raw_format_path_dict["val"] = {"input" : "./ChemProt_Corpus/chemprot_development/chemprot_development_sentences_entities_tagged.tsv",
                      "output" : "./ChemProt_Glue/Raw/val.tsv",
                      "output_sample" : "./ChemProt_Glue/Raw/val_sample.tsv"}

train_sentences_tagged = pd.read_csv(raw_format_path_dict["train"]["input"], sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])
train_sentences_tagged.sample(frac=1).reset_index(drop=True).to_csv(raw_format_path_dict["train"]["output"], sep='\t', columns=['sentence', 'label'], header=False, index=False)

test_sentences_tagged = pd.read_csv(raw_format_path_dict["test"]["input"], sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])
test_sentences_tagged.sample(frac=1).reset_index(drop=True).to_csv(raw_format_path_dict["test"]["output"], sep='\t', columns=['sentence', 'label'], header=False, index=False)

val_sentences_tagged = pd.read_csv(raw_format_path_dict["val"]["input"], sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])
val_sentences_tagged.sample(frac=1).reset_index(drop=True).to_csv(raw_format_path_dict["val"]["output"], sep='\t', columns=['sentence', 'label'], header=False, index=False)
val_sentences_tagged.sample(frac=0.1).reset_index(drop=True).to_csv(raw_format_path_dict["val"]["output_sample"], sep='\t', columns=['sentence', 'label'], header=False, index=False)

"""###One sentence format - random sampling
Select one version of each sentence to avoid overfitting on sentence with many entities combinations.
Select the version to keep randomly and reshuffle the dataset.
"""

one_sentence_random_format_path_dict = {}
one_sentence_random_format_path_dict["train"] = {"input" : "./ChemProt_Corpus/chemprot_training/chemprot_training_sentences_entities_tagged.tsv",
                      "output" : "./ChemProt_Glue/oneSentence_random/train.tsv"}

one_sentence_random_format_path_dict["test"] = {"input" : "./ChemProt_Corpus/chemprot_test_gs/chemprot_test_sentences_entities_tagged_gs.tsv",
                      "output" : "./ChemProt_Glue/oneSentence_random/test.tsv"}

one_sentence_random_format_path_dict["val"] = {"input" : "./ChemProt_Corpus/chemprot_development/chemprot_development_sentences_entities_tagged.tsv",
                      "output" : "./ChemProt_Glue/oneSentence_random/val.tsv"}

train_sentences_tagged = pd.read_csv(one_sentence_random_format_path_dict["train"]["input"], sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])
train_sentences_tagged_selected = train_sentences_tagged.groupby(by=["ID_article", "ID_sentence"]).sample(n=1).reset_index(drop=True)
train_sentences_tagged_selected.to_csv(path_or_buf=one_sentence_random_format_path_dict["train"]["output"], sep='\t', columns=['sentence', 'label'], header=False, index=False)

test_sentences_tagged = pd.read_csv(one_sentence_random_format_path_dict["test"]["input"], sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])
test_sentences_tagged_selected = test_sentences_tagged.groupby(by=["ID_article", "ID_sentence"]).sample(n=1).reset_index(drop=True)
test_sentences_tagged_selected.reset_index().to_csv(path_or_buf=one_sentence_random_format_path_dict["test"]["output"], sep='\t', columns=['sentence', 'label'], header=False, index=False)

val_sentences_tagged = pd.read_csv(one_sentence_random_format_path_dict["val"]["input"], sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])
val_sentences_tagged_selected = val_sentences_tagged.groupby(by=["ID_article", "ID_sentence"]).sample(n=1).reset_index(drop=True)
val_sentences_tagged_selected.reset_index().to_csv(path_or_buf=one_sentence_random_format_path_dict["val"]["output"], sep='\t', columns=['sentence', 'label'], header=False, index=False)

"""###One sentence format - weighted sampling
To compensate for class unbalance, we sample in priority versions that espresses a relation between the two tagged entities.
"""

# The weight function used to prioritize versions supporting relations (with uniform weights on labels 1 or on labels 0)

def compute_weight(cell):
  nb_sentences = cell["nb_sentences"]
  nb_relations = cell["nb_relations"]
  label = cell["label"]

  if nb_relations==0:
    return 1/nb_sentences
  elif label==1:
    return 1/nb_relations
  else:
    return 0

# We store the paths to the different files in a dictionary for easy acces

one_sentence_weighted_format_path_dict = {}
one_sentence_weighted_format_path_dict["train"] = {"input" : "./ChemProt_Corpus/chemprot_training/chemprot_training_sentences_entities_tagged.tsv",
                      "output" : "./ChemProt_Glue/oneSentence_weighted/train.tsv"}

one_sentence_weighted_format_path_dict["test"] = {"input" : "./ChemProt_Corpus/chemprot_test_gs/chemprot_test_sentences_entities_tagged_gs.tsv",
                      "output" : "./ChemProt_Glue/oneSentence_weighted/test.tsv"}

one_sentence_weighted_format_path_dict["val"] = {"input" : "./ChemProt_Corpus/chemprot_development/chemprot_development_sentences_entities_tagged.tsv",
                      "output" : "./ChemProt_Glue/oneSentence_weighted/val.tsv"}

def oneSentence_weighted_select(df):
  # Compute the number of versions for each sentence
  df = df.join(other=df.groupby(by=["ID_article", "ID_sentence"])["label"].count(), on=["ID_article", "ID_sentence"], how='left', rsuffix="_")
  df.rename(columns={"label_":"nb_sentences"}, inplace=True)

  # Compute the number of relations expressed in those versions
  df = df.join(other=df.groupby(by=["ID_article", "ID_sentence"])["label"].sum(), on=["ID_article", "ID_sentence"], how='left', rsuffix="_")
  df.rename(columns={"label_":"nb_relations"}, inplace=True)

  # Apply weight function
  df["weight"] = df.apply(compute_weight, axis=1)

  # Sample the versions using the weights as a probability distribution
  df_selected = df.groupby(by=["ID_article", "ID_sentence"]).sample(n=1, random_state=1, weights=df["weight"]).reset_index(drop=True)

  return df_selected

# Load the data
train_sentences_tagged = pd.read_csv(one_sentence_weighted_format_path_dict["train"]["input"], sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])

# Compute the number of versions for each sentence
train_sentences_tagged = train_sentences_tagged.join(other=train_sentences_tagged.groupby(by=["ID_article", "ID_sentence"])["label"].count(), on=["ID_article", "ID_sentence"], how='left', rsuffix="_")
train_sentences_tagged.rename(columns={"label_":"nb_sentences"}, inplace=True)

# Compute the number of relations expressed in those versions
train_sentences_tagged = train_sentences_tagged.join(other=train_sentences_tagged.groupby(by=["ID_article", "ID_sentence"])["label"].sum(), on=["ID_article", "ID_sentence"], how='left', rsuffix="_")
train_sentences_tagged.rename(columns={"label_":"nb_relations"}, inplace=True)

# Apply weight function
train_sentences_tagged["weight"] = train_sentences_tagged.apply(compute_weight, axis=1)

# Sample the versions using the weights as a probability distribution
train_sentences_tagged_selected = train_sentences_tagged.groupby(by=["ID_article", "ID_sentence"]).sample(n=1, random_state=1, weights=train_sentences_tagged["weight"]).reset_index(drop=True)

# Save the result in the output file
train_sentences_tagged_selected.to_csv(path_or_buf=one_sentence_weighted_format_path_dict["train"]["output"], sep='\t', columns=['sentence', 'label'], header=False, index=False)

# Same process for test dataset

test_sentences_tagged = pd.read_csv(one_sentence_weighted_format_path_dict["test"]["input"], sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])

test_sentences_tagged_selected = oneSentence_weighted_select(test_sentences_tagged)

test_sentences_tagged_selected.to_csv(path_or_buf=one_sentence_weighted_format_path_dict["test"]["output"], sep='\t', columns=['sentence', 'label'], header=False, index=False)

# Same process for validation dataset

eval_sentences_tagged = pd.read_csv(one_sentence_weighted_format_path_dict["val"]["input"], sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])

eval_sentences_tagged_selected = oneSentence_weighted_select(eval_sentences_tagged)

eval_sentences_tagged_selected.to_csv(path_or_buf=one_sentence_weighted_format_path_dict["val"]["output"], sep='\t', columns=['sentence', 'label'], header=False, index=False)

"""### Split Balanced format
We stack the three dataset before preprocessing and splitting again balancing the ratio of label 0/1 in the three output datasets to have the same class balance in train, validation and test
"""

# From folder ChemProt_

train_file_path = './ChemProt_Corpus/chemprot_training/chemprot_training_sentences_entities_tagged.tsv'
test_file_path = './ChemProt_Corpus/chemprot_test_gs/chemprot_test_sentences_entities_tagged_gs.tsv'
eval_file_path = './ChemProt_Corpus/chemprot_development/chemprot_development_sentences_entities_tagged.tsv'

train_sentences_tagged = pd.read_csv(train_file_path, sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])

test_sentences_tagged = pd.read_csv(test_file_path, sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])

eval_sentences_tagged = pd.read_csv(eval_file_path, sep='\t', header=None, names=['ID_article', 'ID_sentence', 'Arg1', 'Arg2', 'sentence', 'label'])

datasets = [train_sentences_tagged, test_sentences_tagged, eval_sentences_tagged]
stacked_sentences_tagged = pd.concat(datasets)
stacked_sentences_tagged.reset_index(inplace=True, drop=True)

stacked_sentences_tagged = stacked_sentences_tagged.join(other=stacked_sentences_tagged.groupby(by=["ID_article", "ID_sentence"])["label"].count(), on=["ID_article", "ID_sentence"], how='left', rsuffix="_")
stacked_sentences_tagged.rename(columns={"label_":"nb_sentences"}, inplace=True)
stacked_sentences_tagged = stacked_sentences_tagged.join(other=stacked_sentences_tagged.groupby(by=["ID_article", "ID_sentence"])["label"].sum(), on=["ID_article", "ID_sentence"], how='left', rsuffix="_")
stacked_sentences_tagged.rename(columns={"label_":"nb_relations"}, inplace=True)

stacked_sentences_tagged["weight"] = stacked_sentences_tagged.apply(compute_weight, axis=1)

stacked_sentences_tagged_selected = stacked_sentences_tagged.groupby(by=["ID_article", "ID_sentence"]).sample(n=1, random_state=1, weights=stacked_sentences_tagged["weight"])

train_selected, tmp = train_test_split(stacked_sentences_tagged_selected, test_size=0.2, stratify=stacked_sentences_tagged_selected['label'])
eval_selected, test_selected = train_test_split(tmp, test_size=0.5, stratify=tmp['label'])

train_selected.reset_index(inplace=True, drop=True)
eval_selected.reset_index(inplace=True, drop=True)
test_selected.reset_index(inplace=True, drop=True)

print("Proportions de relations dans le train set : ", (train_selected['label']==True).mean())
print("Proportions de relations dans le eval set : ", (eval_selected['label']==True).mean())
print("Proportions de relations dans le test set : ", (test_selected['label']==True).mean())

train_selected.to_csv(path_or_buf="./ChemProt_Glue/SplitBalanced/train.tsv", sep='\t', columns=['sentence', 'label'], header=False, index=False)
eval_selected.to_csv(path_or_buf="./ChemProt_Glue/SplitBalanced/val.tsv", sep='\t', columns=['sentence', 'label'], header=False, index=False)
test_selected.to_csv(path_or_buf="./ChemProt_Glue/SplitBalanced/test.tsv", sep='\t', columns=['sentence', 'label'], header=False, index=False)